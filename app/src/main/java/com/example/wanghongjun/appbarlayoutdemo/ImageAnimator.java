package com.example.wanghongjun.appbarlayoutdemo;

import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by wanghongjun on 2018/8/2.
 *
 * 1)管理动画的启动和结束
 * 2)该管理向前移动和向后移动的动画效果
 *
 */

public class ImageAnimator {

    private static final float FACTOR=0.1F; //偏移度

    private final SimpleAdapter mAdapter;  //适配器
    private final ImageView mTargetImage;   //原始图片的控件
    private final ImageView mOutgoningImage;    //渐变图片的控件

    private int mStartPosition; //实际起始位置
    private int mMinPos;    //最小位置
    private int mMaxPos;    //最大位置

    public ImageAnimator(SimpleAdapter adapter,ImageView targetImage,ImageView outgoingImage){
        mAdapter=adapter;
        mTargetImage=targetImage;
        mOutgoningImage=outgoingImage;
    }

    /**
     * 启动动画，之后选择向前或向后滑动
     * @param startPosition
     * @param endPosition
     */
    public void start(int startPosition,int endPosition){
        mStartPosition=startPosition;

        //目标图片资源
        @DrawableRes int incomeId=mAdapter.getDrawable(endPosition);

        //设置动画处理的启动图片
        mOutgoningImage.setImageDrawable(mTargetImage.getDrawable());
        mOutgoningImage.setTranslationX(0f);
        mOutgoningImage.setVisibility(View.VISIBLE);
        mOutgoningImage.setAlpha(1.0F);

        //设置滑动结束的目标图片
        mTargetImage.setImageResource(incomeId);
        mMaxPos=Math.max(startPosition,endPosition);
        mMinPos=Math.min(startPosition,endPosition);
    }

    /**
     * 滑动结束的动画效果
     * @param endPosition
     */
    public void end(int endPosition){
        @DrawableRes int incomeId=mAdapter.getDrawable(endPosition);
        mTargetImage.setTranslationX(0f);

        //滑动并未执行完成，重新返回当前页面
        if (endPosition==mStartPosition){
            mTargetImage.setImageDrawable(mOutgoningImage.getDrawable());
        }else {
            //滑动执行完成，隐藏动画图片
            mTargetImage.setImageResource(incomeId);
            mTargetImage.setAlpha(1f);
            mOutgoningImage.setVisibility(View.GONE);
        }
    }

    /**
     * 向前滚动，比如0->1,offset滚动移动的距离(0->1)目标渐渐淡出
     * @param positionOffset 位置偏移
     */
    public void forward(float positionOffset){
        int width=mTargetImage.getWidth();
        mOutgoningImage.setTranslationX(-positionOffset*(FACTOR*width));
        mTargetImage.setTranslationX((1-positionOffset)*(FACTOR*width));
        mTargetImage.setAlpha(positionOffset);
    }

    /**
     * 向后滚动，比如1->0,offset滚动的距离(1->0)，目标渐渐淡入
     * @param positionOffset
     */
    public void backwards(float positionOffset){
        int width=mTargetImage.getWidth();
        mOutgoningImage.setTranslationX((1-positionOffset)*(FACTOR*width));
        mTargetImage.setTranslationX(-(positionOffset)*(FACTOR*width));

        mTargetImage.setAlpha(1-positionOffset);
    }

    /**
     * 判断位置是否在其中，用于停止动画
     * @param position
     * @return
     */
    public boolean isWidthin(int position){
        return position>=mMinPos && position <mMaxPos;
    }



}
