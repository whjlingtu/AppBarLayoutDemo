package com.example.wanghongjun.appbarlayoutdemo;

import android.support.v4.view.ViewPager;
import android.widget.ImageView;

/**
 * Created by wanghongjun on 2018/8/2.
 */

public class PagerChangerListener implements ViewPager.OnPageChangeListener {

    private ImageAnimator mImageAnimator;   //图片动画器

    private int mCurrentPosition;   //当前位置
    private int mFinalPosition; //最终位置
    private boolean mIsScrolling=false; //是否在滑动

    private PagerChangerListener(ImageAnimator imageAnimator){
        mImageAnimator=imageAnimator;
    }

    public static PagerChangerListener newInstance(SimpleAdapter adapter, ImageView originImage, ImageView outgoingImage){
        ImageAnimator imageAnimator=new ImageAnimator(adapter,originImage,outgoingImage);
        return new PagerChangerListener(imageAnimator);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //以前滑动，现在停止
        if (isFinishedScrolling(position,positionOffset)){
            fininshScroll(position);
        }

        //判断前后滑动是否开始
        if (isStartingScrollToPrevious(position,positionOffset)){
            startScroll(position);
        }else if(isStartScrollToNext(position,positionOffset)){
            startScroll(position+1);
        }

        //向后滚动
        if (isScrollingToNext(position,positionOffset)){
            mImageAnimator.forward(positionOffset);
        }else if(isScrollingToPrevious(position,positionOffset)){
            //先前滚动
            mImageAnimator.backwards(positionOffset);
        }

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * 终止滑动，滑动 && [偏移是0 && 滑动终点] || 动画中
     * @param position
     * @param positionOffset
     * @return
     */
    private boolean isFinishedScrolling(int position,float positionOffset){
        return  mIsScrolling && (positionOffset==0f && position==mFinalPosition)||
                !mImageAnimator.isWidthin(position);
    }

    /**
     * 从禁止到开始滑动，下一个，未滑动 && 位置是当前位置 && 偏移量不是0
     * @param position
     * @param positionOffset
     * @return
     */
    private boolean isStartScrollToNext(int position,float positionOffset){
        return !mIsScrolling && position==mCurrentPosition && positionOffset!=0f;
    }

    /**
     * 从静止到开始滑动，前一个[position-1]
     * @param position
     * @param positionOffset
     * @return
     */
    private boolean isStartingScrollToPrevious(int position,float positionOffset){
        return !mIsScrolling && position!=mCurrentPosition && positionOffset!=0f;
    }

    /**
     * 开始滚动,向后
     * @param position
     * @param positionOffset
     * @return
     */
    private boolean isScrollingToNext(int position,float positionOffset){
        return mIsScrolling && position==mCurrentPosition && positionOffset!=0f;
    }

    /**
     * 开始滚动，向前
     * @param position
     * @param positionOffset
     * @return
     */
    private boolean isScrollingToPrevious(int position,float positionOffset){
        return mIsScrolling && position!=mCurrentPosition && positionOffset!=0f;
    }

    /**
     * 开始滑动、滚动开始、结束位置是position[向前滚动会自动减1]，
     * 动画从当前位置到结束位置
     * @param position
     * @return
     */
    private void startScroll(int position){
        mIsScrolling=true;
        mFinalPosition=position;
        //开始滚动动画
        mImageAnimator.start(mCurrentPosition,position);
    }

    /**
     * 如果正在滚动、结束时，固定position位置、停止滚动，
     * 调动截至动画
     * @param position
     */
    private void fininshScroll(int position){
        if (mIsScrolling){
            mCurrentPosition=position;
            mIsScrolling=false;
            mImageAnimator.end(position);
        }
    }





}
