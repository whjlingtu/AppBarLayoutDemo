package com.example.wanghongjun.appbarlayoutdemo;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by wanghongjun on 2018/8/1.
 */

public class SimpleAdapter extends FragmentPagerAdapter {
    //展示信息
    private static final Section[] SECTIONS={
            new Section("Tiffany",R.mipmap.tiffany),
            new Section("Taeyeon",R.mipmap.teayeon),
            new Section("Yoona",R.mipmap.yoona)
    };


    public SimpleAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return SimpleFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return SECTIONS.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position>=0 && position<SECTIONS.length){
            return SECTIONS[position].getTitle();
        }
        return null;
    }

    public @DrawableRes int getDrawable(int position){
        if (position>=0 && position<SECTIONS.length){
            return SECTIONS[position].getDrawable();
        }
        return -1;
    }


    private static final class Section{
        private final String mTile; //标题
        private final @DrawableRes int mDrawable; //图片

        public Section(String titile,int drawable){
            mTile=titile;
            mDrawable= drawable;
        }

        public String getTitle(){
            return mTile;
        }

        public @DrawableRes int getDrawable(){
            return mDrawable;
        }
    }
}
