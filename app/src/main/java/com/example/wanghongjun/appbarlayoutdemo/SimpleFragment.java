package com.example.wanghongjun.appbarlayoutdemo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wanghongjun on 2018/8/1.
 */

public class SimpleFragment extends Fragment {

    private static final String ARG_SELECTION_NUM = "arg_selection_num";

    //显示文本信息
    private static final int[] TEXTS = {
            R.string.tiffany_text,
            R.string.teayon_text,
            R.string.yoona_text
    };
    @BindView(R.id.main_tv_text)
    TextView mainTvText;

    public SimpleFragment() {

    }

    /**
     * 通过静态接口创建Fragment,规范参数的使用
     *
     * @param selectionNum
     * @return
     */
    public static SimpleFragment newInstance(int selectionNum) {
        SimpleFragment simpleFragment = new SimpleFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SELECTION_NUM, selectionNum);
        simpleFragment.setArguments(bundle);
        return simpleFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mainTvText.setText(TEXTS[getArguments().getInt(ARG_SELECTION_NUM)]);
    }
}
